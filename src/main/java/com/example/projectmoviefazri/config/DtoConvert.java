package com.example.projectmoviefazri.config;

import org.modelmapper.ModelMapper;

public class DtoConvert {
	ModelMapper modelMapper = new ModelMapper();
	
	 public Object convertToDTO(Object o) {
		 Object o1 = modelMapper.map(o, Object.class);
         return o1;
	 }
	    
	 public Object convertToEntity(Object o) {
    	 Object o1 = modelMapper.map(o, Object.class);
         return o1;
    }
}
