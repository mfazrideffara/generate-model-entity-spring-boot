package com.example.projectmoviefazri.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.projectmoviefazri.config.DtoConvert;
import com.example.projectmoviefazri.model.Director;
import com.example.projectmoviefazri.model.dto.DirectorDto;
import com.example.projectmoviefazri.repository.DirectorRepository;

@RestController
@RequestMapping("/api")
public class DirectorController {
	 
	 ModelMapper modelMapper = new ModelMapper();
	 
	 DtoConvert dtoConvert = new DtoConvert();
	 
	 @Autowired
	 DirectorRepository directorRepository;
	 
//	 public DirectorDto convertToDTO(Director director) {
//		 DirectorDto directorDto = modelMapper.map(director, DirectorDto.class);
//         return directorDto;
//	 }
//	    
//    private Director convertToEntity(DirectorDto directorDto) {
//    	Director director = modelMapper.map(directorDto, Director.class);
//        return director;
//    }
		 
	 //Get All Director
	 @GetMapping("/director/readAll")
	 public HashMap<String, Object> getAllDirector() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<DirectorDto> listDirectors = new ArrayList<DirectorDto>();
		for(Director d : directorRepository.findAll()) {
			DirectorDto directorDto = (DirectorDto) dtoConvert.convertToDTO(d);
			listDirectors.add(directorDto);
		}
		
		String message;
        if(listDirectors.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listDirectors.size());
    	showHashMap.put("Data", listDirectors);
		
		return showHashMap;
	 }
	 
	 // Read Director By ID
	 @GetMapping("/director/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Director director = directorRepository.findById(id)
				.orElse(null);
		DirectorDto directorDto = (DirectorDto) dtoConvert.convertToDTO(director);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", directorDto);
		return showHashMap;
	}
	 
	// Create a new Director
	@PostMapping("/director/add")
	public HashMap<String, Object> createDirector(@Valid @RequestBody ArrayList<DirectorDto> directorDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<DirectorDto> listDirectors = directorDto;
    	String message;
    	
    	for(DirectorDto d : listDirectors) {
    		Director director = (Director) dtoConvert.convertToEntity(d);
    		directorRepository.save(director);
    	}
    
    	if(listDirectors == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listDirectors.size());
    	showHashMap.put("Data", listDirectors);
    	
    	return showHashMap;
    }
	
	// Update a Director
    @PutMapping("/director/update/{id}")
    public HashMap<String, Object> updateDirector(@PathVariable(value = "id") Long id,
            @Valid @RequestBody DirectorDto directorDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int directorId = id.intValue();
    	List<Director> listDirectors = directorRepository.findAll();
    	
    	for(Director d : listDirectors) {
    		if(d.getDirId() == directorId) {
    			if(directorDetails.getDirFname() == 0) {
    				directorDetails.setDirFname(listDirectors.get(directorId).getDirFname());
    	    	}
    	    	if(directorDetails.getDirLname() == 0) {
    	    		directorDetails.setDirLname(listDirectors.get(directorId).getDirLname());
    	    	}
    		}
    	}	
    	
    	Director director = directorRepository.findById(id)
    			 .orElse(null);

    	director.setDirFname(directorDetails.getDirFname());
    	director.setDirLname(directorDetails.getDirLname());
    	
    	Director updateDirector = directorRepository.save(director);
    	
    	List<Director> resultList = new ArrayList<Director>();
    	resultList.add(updateDirector);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a Director
    @DeleteMapping("/director/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Director director = directorRepository.findById(id)
    			.orElse(null);

    	directorRepository.delete(director);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", director);
    	return showHashMap;
    }
}
	 
