package com.example.projectmoviefazri.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.projectmoviefazri.config.DtoConvert;
import com.example.projectmoviefazri.model.Movie;
import com.example.projectmoviefazri.model.dto.MovieDto;
import com.example.projectmoviefazri.repository.MovieRepository;

@RestController
@RequestMapping("/api")
public class MovieController {
	 
	 ModelMapper modelMapper = new ModelMapper();
	 
	 DtoConvert dtoConvert = new DtoConvert();
	 
	 @Autowired
	 MovieRepository movieRepository;
	 
//	 public MovieDto convertToDTO(Movie movie) {
//		 MovieDto movieDto = modelMapper.map(movie, MovieDto.class);
//	     return movieDto;
//	 }
//
//	 private Movie convertToEntity(MovieDto movieDto) {
//		 Movie movie = modelMapper.map(movieDto, Movie.class);
//	     return movie;
//	 }
	 
	 //Get All Movie
	 @GetMapping("/movie/readAll")
	 public HashMap<String, Object> getAllMovie() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<MovieDto> listMovies = new ArrayList<MovieDto>();
		for(Movie m : movieRepository.findAll()) {
			MovieDto movieDto = (MovieDto) dtoConvert.convertToDTO(m);
			listMovies.add(movieDto);
		}
		
		String message;
        if(listMovies.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listMovies.size());
    	showHashMap.put("Data", listMovies);
		
		return showHashMap;
	 }
	 
	 // Read Movie By ID
	 @GetMapping("/movie/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Movie movie = movieRepository.findById(id)
				.orElse(null);
		MovieDto movieDto = (MovieDto) dtoConvert.convertToDTO(movie);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", movieDto);
		return showHashMap;
	}
	 
	// Create a new Movie
	@PostMapping("/movie/add")
	public HashMap<String, Object> createMovie(@Valid @RequestBody ArrayList<MovieDto> movieDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<MovieDto> listMovies = movieDto;
    	String message;
    	
    	for(MovieDto m : listMovies) {
    		Movie Movie = (Movie) dtoConvert.convertToEntity(m);
    		movieRepository.save(Movie);
    	}
    
    	if(listMovies == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listMovies.size());
    	showHashMap.put("Data", listMovies);
    	
    	return showHashMap;
    }
	
	// Update a Movie
    @PutMapping("/movie/update/{id}")
    public HashMap<String, Object> updateMovie(@PathVariable(value = "id") Long id,
            @Valid @RequestBody MovieDto movieDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int movieId = id.intValue();
    	List<Movie> listMovies = movieRepository.findAll();
    	
    	for(Movie g : listMovies) {
    		if(g.getMovId() == movieId) {
    			if(movieDetails.getMovTitle() == null) {
    				movieDetails.setMovTitle(listMovies.get(movieId).getMovTitle());
    	    	}
    			if(movieDetails.getMovYear() == 0) {
    				movieDetails.setMovYear(listMovies.get(movieId).getMovYear());
    	    	}
    			if(movieDetails.getMovTime() == 0) {
    				movieDetails.setMovTime(listMovies.get(movieId).getMovTime());
    	    	}
    			if(movieDetails.getMovLang() == null) {
    				movieDetails.setMovLang(listMovies.get(movieId).getMovLang());
    	    	}
    			if(movieDetails.getMovDtRel() == null) {
    				movieDetails.setMovDtRel(listMovies.get(movieId).getMovDtRel());
    	    	}
    			if(movieDetails.getMovRelCountry() == null) {
    				movieDetails.setMovRelCountry(listMovies.get(movieId).getMovRelCountry());
    	    	}
    		}
    	}	
    	
    	Movie movie = movieRepository.findById(id)
    			 .orElse(null);

    	movie.setMovTitle(movieDetails.getMovTitle());
    	movie.setMovYear(movieDetails.getMovYear());
    	movie.setMovTime(movieDetails.getMovTime());
    	movie.setMovLang(movieDetails.getMovLang());
    	movie.setMovDtRel(movieDetails.getMovDtRel());
    	movie.setMovRelCountry(movieDetails.getMovRelCountry());
    	
    	Movie updateMovie = movieRepository.save(movie);
    	
    	List<Movie> resultList = new ArrayList<Movie>();
    	resultList.add(updateMovie);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a Movie
    @DeleteMapping("/movie/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Movie movie = movieRepository.findById(id)
    			.orElse(null);

    	movieRepository.delete(movie);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", movie);
    	return showHashMap;
    }
}
	 
