package com.example.projectmoviefazri.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.projectmoviefazri.config.DtoConvert;
import com.example.projectmoviefazri.model.Reviewer;
import com.example.projectmoviefazri.model.dto.ReviewerDto;
import com.example.projectmoviefazri.repository.ReviewerRepository;

@RestController
@RequestMapping("/api")
public class ReviewerController {
	 
	 ModelMapper modelMapper = new ModelMapper();
	 
	 DtoConvert dtoConvert = new DtoConvert();
	 
	 @Autowired
	 ReviewerRepository reviewerRepository;
	 
//	 public ReviewerDto convertToDTO(Reviewer reviewer) {
//		 ReviewerDto reviewerDto = modelMapper.map(reviewer, ReviewerDto.class);
//	     return reviewerDto;
//	 }
//
//	 private Reviewer convertToEntity(ReviewerDto reviewerDto) {
//		 Reviewer reviewer = modelMapper.map(reviewerDto, Reviewer.class);
//	     return reviewer;
//	 }
	 
	 //Get All Reviewer
	 @GetMapping("/reviewer/readAll")
	 public HashMap<String, Object> getAllReviewer() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<ReviewerDto> listReviewers = new ArrayList<ReviewerDto>();
		for(Reviewer r : reviewerRepository.findAll()) {
			ReviewerDto reviewerDto = (ReviewerDto) dtoConvert.convertToDTO(r);
			listReviewers.add(reviewerDto);
		}
		
		String message;
        if(listReviewers.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listReviewers.size());
    	showHashMap.put("Data", listReviewers);
		
		return showHashMap;
	 }
	 
	 // Read Reviewer By ID
	 @GetMapping("/reviewer/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Reviewer reviewer = reviewerRepository.findById(id)
				.orElse(null);
		ReviewerDto reviewerDto = (ReviewerDto) dtoConvert.convertToDTO(reviewer);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", reviewerDto);
		return showHashMap;
	}
	 
	// Create a new Reviewer
	@PostMapping("/reviewer/add")
	public HashMap<String, Object> createReviewer(@Valid @RequestBody ArrayList<ReviewerDto> reviewerDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<ReviewerDto> listReviewers = reviewerDto;
    	String message;
    
    	for(ReviewerDto r : listReviewers) {
    		Reviewer reviewer = (Reviewer) dtoConvert.convertToEntity(r);
    		reviewerRepository.save(reviewer);
    	}
    
    	if(listReviewers == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listReviewers.size());
    	showHashMap.put("Data", listReviewers);
    	
    	return showHashMap;
    }
	
	// Update a Reviewer
    @PutMapping("/reviewer/update/{id}")
    public HashMap<String, Object> updateReviewer(@PathVariable(value = "id") Long id,
            @Valid @RequestBody ReviewerDto reviewerDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int reviewerId = id.intValue();
    	List<Reviewer> listReviewers = reviewerRepository.findAll();
    	
    	for(Reviewer r : listReviewers) {
    		if(r.getRevId() == reviewerId) {
    			if(reviewerDetails.getRevName() == null) {
    				reviewerDetails.setRevName(listReviewers.get(reviewerId).getRevName());
    	    	}
    		}
    	}	
    	
    	Reviewer reviewer = reviewerRepository.findById(id)
    			 .orElse(null);

    	reviewer.setRevName(reviewerDetails.getRevName());
    	
    	Reviewer updateReviewer = reviewerRepository.save(reviewer);
    	
    	List<Reviewer> resultList = new ArrayList<Reviewer>();
    	resultList.add(updateReviewer);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a Reviewer
    @DeleteMapping("/reviewer/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Reviewer reviewer = reviewerRepository.findById(id)
    			.orElse(null);

    	reviewerRepository.delete(reviewer);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", reviewer);
    	return showHashMap;
    }
}
	 
