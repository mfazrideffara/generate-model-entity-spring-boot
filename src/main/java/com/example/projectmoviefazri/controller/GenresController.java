package com.example.projectmoviefazri.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.projectmoviefazri.config.DtoConvert;
import com.example.projectmoviefazri.model.Genres;
import com.example.projectmoviefazri.model.dto.GenresDto;
import com.example.projectmoviefazri.repository.GenresRepository;

@RestController
@RequestMapping("/api")
public class GenresController {
	
	 ModelMapper modelMapper = new ModelMapper();
	 
	 DtoConvert dtoConvert = new DtoConvert();
	 
	 @Autowired
	 GenresRepository genresRepository;
	 
//	 public GenresDto convertToDTO(Genres genres) {
//		 GenresDto genresDto = modelMapper.map(genres, GenresDto.class);
//	     return genresDto;
//	 }
//    
//	 private Genres convertToEntity(GenresDto genresDto) {
//		 Genres genres = modelMapper.map(genresDto, Genres.class);
//	     return genres;
//	 }
	 
	 //Get All Genres
	 @GetMapping("/genres/readAll")
	 public HashMap<String, Object> getAllGenres() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<GenresDto> listGenres = new ArrayList<GenresDto>();
		for(Genres g : genresRepository.findAll()) {
			GenresDto genresDto = (GenresDto) dtoConvert.convertToDTO(g);
			listGenres.add(genresDto);
		}
		
		String message;
        if(listGenres.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listGenres.size());
    	showHashMap.put("Data", listGenres);
		
		return showHashMap;
	 }
	 
	 // Read Genres By ID
	 @GetMapping("/genres/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Genres genres = genresRepository.findById(id)
				.orElse(null);
		GenresDto genresDto = (GenresDto) dtoConvert.convertToDTO(genres);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", genresDto);
		return showHashMap;
	}
	 
	// Create a new Genres
	@PostMapping("/genres/add")
	public HashMap<String, Object> createGenres(@Valid @RequestBody ArrayList<GenresDto> genresDto) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<GenresDto> listGenres = genresDto;
    	String message;
    	
    	for(GenresDto g : listGenres) {
    		Genres genres = (Genres) dtoConvert.convertToEntity(g);
    		genresRepository.save(genres);
    	}
    
    	if(listGenres == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listGenres.size());
    	showHashMap.put("Data", listGenres);
    	
    	return showHashMap;
    }
	
	// Update a Genres
    @PutMapping("/genres/update/{id}")
    public HashMap<String, Object> updateGenres(@PathVariable(value = "id") Long id,
            @Valid @RequestBody GenresDto genresDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int genresId = id.intValue();
    	List<Genres> listGenress = genresRepository.findAll();
    	
    	for(Genres g : listGenress) {
    		if(g.getGenId() == genresId) {
    			if(genresDetails.getGenTitle() == 0) {
    				genresDetails.setGenTitle(listGenress.get(genresId).getGenTitle());
    	    	}
    		}
    	}	
    	
    	Genres Genres = genresRepository.findById(id)
    			 .orElse(null);

    	Genres.setGenTitle(genresDetails.getGenTitle());
    	
    	Genres updateGenres = genresRepository.save(Genres);
    	
    	List<Genres> resultList = new ArrayList<Genres>();
    	resultList.add(updateGenres);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a Genres
    @DeleteMapping("/genres/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Genres genres = genresRepository.findById(id)
    			.orElse(null);

    	genresRepository.delete(genres);

        showHashMap.put("Messages", "Delete Data Success!");
        showHashMap.put("Delete data :", genres);
    	return showHashMap;
    }
}
	 
