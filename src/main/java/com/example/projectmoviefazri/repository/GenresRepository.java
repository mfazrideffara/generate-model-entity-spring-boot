package com.example.projectmoviefazri.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.projectmoviefazri.model.Genres;

public interface GenresRepository extends JpaRepository<Genres, Long>{

}
