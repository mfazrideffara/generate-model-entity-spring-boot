package com.example.projectmoviefazri.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.projectmoviefazri.model.Actor;

public interface ActorRepository extends JpaRepository<Actor, Long>{

}
