package com.example.projectmoviefazri.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.projectmoviefazri.model.MovieCast;

public interface MovieCastRepository extends JpaRepository<MovieCast, Long>{

}
