package com.example.projectmoviefazri.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.projectmoviefazri.model.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long>{

}
