package com.example.projectmoviefazri.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.projectmoviefazri.model.Reviewer;

public interface ReviewerRepository extends JpaRepository<Reviewer, Long>{

}
