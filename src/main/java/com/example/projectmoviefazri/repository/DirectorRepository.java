package com.example.projectmoviefazri.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.projectmoviefazri.model.Director;

public interface DirectorRepository extends JpaRepository<Director, Long>{

}
