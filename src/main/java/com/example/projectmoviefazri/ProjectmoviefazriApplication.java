package com.example.projectmoviefazri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectmoviefazriApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectmoviefazriApplication.class, args);
	}

}
