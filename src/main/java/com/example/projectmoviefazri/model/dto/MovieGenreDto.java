package com.example.projectmoviefazri.model.dto;

import java.io.Serializable;

public class MovieGenreDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private MovieDto movie;
	private GenresDto genre;
	
	public MovieGenreDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MovieGenreDto(MovieDto movie, GenresDto genre) {
		super();
		this.movie = movie;
		this.genre = genre;
	}
	public MovieDto getMovie() {
		return movie;
	}
	public GenresDto getGenre() {
		return genre;
	}
	public void setMovie(MovieDto movie) {
		this.movie = movie;
	}
	public void setGenre(GenresDto genre) {
		this.genre = genre;
	}
	
}
