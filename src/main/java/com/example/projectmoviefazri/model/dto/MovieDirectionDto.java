package com.example.projectmoviefazri.model.dto;

import java.io.Serializable;

public class MovieDirectionDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private MovieDto movie;
	private DirectorDto diretor;
	
	public MovieDto getMovie() {
		return movie;
	}
	public DirectorDto getDirector() {
		return diretor;
	}
	public void setMovie(MovieDto movie) {
		this.movie = movie;
	}
	public void setDirector(DirectorDto diretor) {
		this.diretor = diretor;
	}
	
}
