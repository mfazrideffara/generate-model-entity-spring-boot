package com.example.projectmoviefazri.model.dto;

import java.io.Serializable;

public class GenresDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private int genId;
	private char genTitle;
	
	public GenresDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GenresDto(int genId, char genTitle) {
		super();
		this.genId = genId;
		this.genTitle = genTitle;
	}

	public int getGenId() {
		return genId;
	}

	public char getGenTitle() {
		return genTitle;
	}

	public void setGenId(int genId) {
		this.genId = genId;
	}

	public void setGenTitle(char genTitle) {
		this.genTitle = genTitle;
	}
	
}
