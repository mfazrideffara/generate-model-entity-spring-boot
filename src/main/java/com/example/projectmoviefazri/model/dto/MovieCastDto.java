package com.example.projectmoviefazri.model.dto;

import java.io.Serializable;

public class MovieCastDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private ActorDto actor;
	private MovieDto movie;
	private String role;
	
	public MovieCastDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MovieCastDto(ActorDto actor, MovieDto movie, String role) {
		super();
		this.actor = actor;
		this.movie = movie;
		this.role = role;
	}
	public ActorDto getActor() {
		return actor;
	}
	public MovieDto getMovie() {
		return movie;
	}
	public String getRole() {
		return role;
	}
	public void setActor(ActorDto actor) {
		this.actor = actor;
	}
	public void setMovie(MovieDto movie) {
		this.movie = movie;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
}
