package com.example.projectmoviefazri.model.dto;

import java.io.Serializable;

public class ActorDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int actId;
	private char actFname;
	private char actLname;
	private char actGender;
	
	public ActorDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ActorDto(int actId, char actFname, char actLname, char actGender) {
		super();
		this.actId = actId;
		this.actFname = actFname;
		this.actLname = actLname;
		this.actGender = actGender;
	}

	public int getActId() {
		return actId;
	}

	public char getActFname() {
		return actFname;
	}

	public char getActLname() {
		return actLname;
	}

	public char getActGender() {
		return actGender;
	}

	public void setActId(int actId) {
		this.actId = actId;
	}

	public void setActFname(char actFname) {
		this.actFname = actFname;
	}

	public void setActLname(char actLname) {
		this.actLname = actLname;
	}

	public void setActGender(char actGender) {
		this.actGender = actGender;
	}
	
	
}
