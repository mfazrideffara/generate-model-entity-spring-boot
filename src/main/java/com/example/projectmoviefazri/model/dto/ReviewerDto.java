package com.example.projectmoviefazri.model.dto;

import java.io.Serializable;

public class ReviewerDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int revId;
	private char revName;
	
	public ReviewerDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReviewerDto(int revId, char revName) {
		super();
		this.revId = revId;
		this.revName = revName;
	}

	public int getRevId() {
		return revId;
	}

	public char getRevName() {
		return revName;
	}

	public void setRevId(int revId) {
		this.revId = revId;
	}

	public void setRevName(char revName) {
		this.revName = revName;
	}
}
