package com.example.projectmoviefazri.model.dto;

import java.io.Serializable;

public class RatingDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private ReviewerDto reviewer;
	private MovieDto movie;
	private int reviewerStars;
	private int numberOfRatings;
	
	public ReviewerDto getReviewer() {
		return reviewer;
	}
	public MovieDto getMovie() {
		return movie;
	}
	public int getReviewerStars() {
		return reviewerStars;
	}
	public int getNumberOfRatings() {
		return numberOfRatings;
	}
	public void setReviewer(ReviewerDto reviewer) {
		this.reviewer = reviewer;
	}
	public void setMovie(MovieDto movie) {
		this.movie = movie;
	}
	public void setReviewerStars(int reviewerStars) {
		this.reviewerStars = reviewerStars;
	}
	public void setNumberOfRatings(int numberOfRatings) {
		this.numberOfRatings = numberOfRatings;
	}
	
	
}
