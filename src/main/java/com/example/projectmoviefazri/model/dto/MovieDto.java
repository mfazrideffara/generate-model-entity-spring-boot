package com.example.projectmoviefazri.model.dto;

import java.io.Serializable;
import java.util.Date;

public class MovieDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private long movId;
	private char movTitle;
	private Integer movYear;
	private Integer movTime;
	private char movLang;
	private Date movDtRel;
	private char movRelCountry;
	
	public MovieDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MovieDto(long movId, char movTitle, Integer movYear, Integer movTime, char movLang, Date movDtRel,
			char movRelCountry) {
		super();
		this.movId = movId;
		this.movTitle = movTitle;
		this.movYear = movYear;
		this.movTime = movTime;
		this.movLang = movLang;
		this.movDtRel = movDtRel;
		this.movRelCountry = movRelCountry;
	}

	public long getMovId() {
		return movId;
	}

	public char getMovTitle() {
		return movTitle;
	}

	public Integer getMovYear() {
		return movYear;
	}

	public Integer getMovTime() {
		return movTime;
	}

	public char getMovLang() {
		return movLang;
	}

	public Date getMovDtRel() {
		return movDtRel;
	}

	public char getMovRelCountry() {
		return movRelCountry;
	}

	public void setMovId(long movId) {
		this.movId = movId;
	}

	public void setMovTitle(char movTitle) {
		this.movTitle = movTitle;
	}

	public void setMovYear(Integer movYear) {
		this.movYear = movYear;
	}

	public void setMovTime(Integer movTime) {
		this.movTime = movTime;
	}

	public void setMovLang(char movLang) {
		this.movLang = movLang;
	}

	public void setMovDtRel(Date movDtRel) {
		this.movDtRel = movDtRel;
	}

	public void setMovRelCountry(char movRelCountry) {
		this.movRelCountry = movRelCountry;
	}
	
}
