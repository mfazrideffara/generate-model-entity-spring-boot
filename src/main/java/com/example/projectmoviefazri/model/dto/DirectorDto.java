package com.example.projectmoviefazri.model.dto;

import java.io.Serializable;

public class DirectorDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private int dirId;
	private char dirFname;
	private char dirLname;
	
	public DirectorDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DirectorDto(int dirId, char dirFname, char dirLname) {
		super();
		this.dirId = dirId;
		this.dirFname = dirFname;
		this.dirLname = dirLname;
	}

	public int getDirId() {
		return dirId;
	}

	public char getDirFname() {
		return dirFname;
	}

	public char getDirLname() {
		return dirLname;
	}

	public void setDirId(int dirId) {
		this.dirId = dirId;
	}

	public void setDirFname(char dirFname) {
		this.dirFname = dirFname;
	}

	public void setDirLname(char dirLname) {
		this.dirLname = dirLname;
	}
	
}
